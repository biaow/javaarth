import java.util.Random;
import java.util.Scanner;

/**
 * Created by asus on 2017/6/3.
 */
public class LevelTwo extends SetNum {
    Scanner in = new Scanner(System.in);
    Random out = new Random();
    int count = 0;
    int first = getA();
    int last = getA();
    int first1 = getA();
    int last2 = getA();

    RationalNumber score = new RationalNumber(first, last);
    RationalNumber score2 = new RationalNumber(first1, last2);

    public void add() {
        System.out.println(first + "/" + last + "+" + first1 + "/" + last2 + "= ?");
        String result = in.nextLine();
        if (result.equalsIgnoreCase(String.valueOf(score.add(score2)))) {
            System.out.println("TRUE");
            count++;
        } else {
            System.out.println("Wrong");
            System.out.println("The true is:" + score.add(score2));
        }
    }

    public void sub() {
        System.out.println(first + "/" + last + "-" + first1 + "/" + last2 + "= ?");
        String result = in.nextLine();
        if (result.equalsIgnoreCase(String.valueOf(score.subtract(score2)))) {
            System.out.println("TRUE");
            count++;
        } else {
            System.out.println("Wrong");
            System.out.println("The true is:" + score.subtract(score2));
        }
    }

    public void mul() {
        System.out.println(first + "/" + last + "*" + first1 + "/" + last2 + "= ?");
        String result = in.nextLine();
        if (result.equalsIgnoreCase(String.valueOf(score.multiply(score2)))) {
            System.out.println("TRUE");
            count++;
        } else {
            System.out.println("Wrong");
            System.out.println("The true is:" + score.multiply(score2));
        }
    }

    public void div() {
        System.out.println(first + "/" + last + "/" + first1 + "/" + last2 + "= ?");
        String result = in.nextLine();
        if (result.equalsIgnoreCase(String.valueOf(score.divide(score2)))) {
            System.out.println("TRUE");
            count++;
        } else {
            System.out.println("Wrong");
            System.out.println("The true is:" + score.divide(score2));
        }
    }

    public void choose() {
        LevelOne one = new LevelOne();
        LevelTwo two = new LevelTwo();
        int num = out.nextInt(5);
        if (num == 0) {
            two.add();
        } else if (num == 1) {
            two.sub();
        } else if (num == 2) {
            two.mul();
        } else if (num == 3) {
            two.div();
        } else if (num == 4) {
            one.OpOne();
        }
    }

    public void project() {
        Scanner scan = new Scanner(System.in);
        LevelTwo two = new LevelTwo();
        Postfix b = new Postfix();
        System.out.println("How many do you need?");
        int num = scan.nextInt();
        for (int i = 0; i < num; i++) {
            two.choose();
        }

    }

    public String Pro(){
        return getA()+" "+"/"+" "+getA()+" "+Operator[getC()]+" "+getA()+" "+"/"+" "+getA();
    }
}



