/**
 * Created by asus on 2017/5/13.
 */
import java.text.DecimalFormat;
import java.util.*;
public class Postfix {
    //用于记录操作符
    private static LinkedList<String> operators=new LinkedList();
    //用于记录输出
    private static LinkedList<String> output=new LinkedList();
    //用于展示后缀表达式
    private static StringBuilder sb=new StringBuilder();

    //计算
    public String Count(String s){
        LinkedList<String> list = new LinkedList();
        for (int i = 0;i<s.length();i++){
            list.add(String.valueOf(s.charAt(i)));
        }
        return transferToPostfix(list);

    }

    //中缀表达式转为后缀表达式
    public LinkedList blank(LinkedList<String> list){
        for (int i =0;i<list.size();i++){
            list.removeFirstOccurrence(" ");
        }
        return list;
    }
    public String transferToPostfix(LinkedList<String> list){
        blank(list);
        Iterator<String> it=list.iterator();
        while (it.hasNext()) {
            String s = it.next();
            if (isOperator(s)) {
                if (operators.isEmpty()) {
                    operators.push(s);
                }
                else {

                    if (priority((operators.peek()).charAt(0))<=priority(s.charAt(0))&&!s.equals(")")) {
                        operators.push(s);
                    }
                    else if(!s.equals(")")&&priority((operators.peek()).charAt(0))>priority(s.charAt(0))){
                        while (operators.size()!=0&&priority((operators.peek()).charAt(0))>=priority(s.charAt(0))
                                &&!operators.peek().equals("(")) {
                            if (!operators.peek().equals("(")) {
                                String operator=operators.pop();
                                sb.append(operator).append(" ");
                                output.push(operator);
                            }
                        }
                        operators.push(s);
                    }

                    else if (s.equals(")")) {
                        while (!operators.peek().equals("(")) {
                            String operator=operators.pop();
                            sb.append(operator).append(" ");
                            output.push(operator);
                        }

                        operators.pop();
                    }
                }
            }

            else {
                sb.append(s).append(" ");
                output.push(s);
            }
        }
        if (!operators.isEmpty()) {
            Iterator<String> iterator=operators.iterator();
            while (iterator.hasNext()) {
                String operator=iterator.next();
                sb.append(operator).append(" ");
                output.push(operator);
                iterator.remove();
            }
        }

       return Calculate();
        //Collections.reverse(output);
    }

    public String Calculate(){
        LinkedList<String> mList=new LinkedList();
        DecimalFormat fmt = new DecimalFormat("0.##");
        String[] postStr=sb.toString().split(" ");
        for (String s:postStr) {
            if (isOperator(s)){
                if (!mList.isEmpty()){
                    double num1= Double.parseDouble(mList.pop());
                    double num2= Double.parseDouble(mList.pop());

                    double newNum= evalSingleOp(num2,num1,s.charAt(0));
                    mList.push(String.valueOf(newNum));
                }
            }
            else {
                mList.push(s);
            }
        }
        if (!mList.isEmpty()){
           return fmt.format(Double.parseDouble(mList.pop()));
        }
        return fmt.format(Double.parseDouble(mList.pop()));
    }

    public boolean isOperator(String oper){
        if (oper.equals("+")||oper.equals("-")||oper.equals("/")||oper.equals("*")
                ||oper.equals("(")||oper.equals(")")) {
            return true;
        }
        return false;
    }

    public int priority(char s){
        switch (s) {
            case '+':return 1;
            case '-':return 1;
            case '*':return 2;
            case '/':return 2;
            case '(':return 3;
            case ')':return 3;
            default :return 0;
        }
    }

    public double evalSingleOp(double num1, double num2, char operator){
        double result = 0;
        switch (operator){
            case '+':
                result=num1+num2;
                break;
            case '-':
                result= num1-num2;
                break;
            case '*':
                result= num1*num2;
                break;
            case '/':result= num1/num2;
                break;
            default :return 0;
        }
        return result;
    }

    public static void main (String args []){
        Postfix a = new Postfix();
        LevelThree b = new LevelThree();

            System.out.println(a.Count(b.JudgeAllpart()));
    }


    }




