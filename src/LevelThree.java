import java.util.Scanner;

/**
 * Created by asus on 2017/5/18.
 */
public class LevelThree extends SetNum{
    Scanner in = new Scanner(System.in);

    Postfix b = new Postfix();
    int count = 0;
    public String Front(int m){
        String result;
        switch (m){

            case 0:
                result = getA()+" "+Operator[getB()]+" ";

                break;
            case 1:
                result = getA()+" "+Operator[getB()]+" "+getA()+" "+Operator[getB()]+" ";

                break;
            default:
                result = getA()+" "+Operator[getB()]+" "+getA()+" "+Operator[getB()]+" "+getA()+" "+Operator[getB()]+" ";
        }
        return result;
    }
    public String Middle(int m){
        String result;
        switch (m){
            case 0:
                result = getA()+" ";
                break;
            case 1:
                result ="("+" "+getA()+" "+Operator[getB()]+" "+getA()+")"+" ";
                break;
                default:
                    result = "("+" "+getA()+" "+Operator[getB()]+" "+"("+" "+getA()+" "+Operator[getB()]+" "+getA()+")"+" "+Operator[getB()]+" "+getA()+")"+" ";
        }
        return result;
    }
    public String End(int m){
        String result;
        switch (m){
            case 0:
                result = Operator[getB()]+" "+getA();
                break;
            case 1:
                result = Operator[getB()]+" "+getA()+" "+Operator[getB()]+" "+getA();
                break;
                default:
                    result = Operator[getB()]+" "+getA()+" "+Operator[getB()]+" "+getA()+" "+Operator[getB()]+" "+getA();
        }
        return result;
    }
    public String Allpart(){
        String result;
        switch (getC()){
            case 0:
                result = Front(getC())+Middle(getC());
                break;
            case 1:
                result = Middle(getC())+Front(getC());
                default:
                    result = Front(getC())+Middle(getC())+End(getC());
        }
        return result;
    }
    public String JudgeAllpart(){
        String result =Allpart();
        for(int i = 0;i<result.length();i++){
            if ((result.charAt(i)=='/')&&(result.charAt(i+1)=='0')){
                result = Allpart();
            }
        }
        return result;
    }
    public void Level(){
        LevelThree a = new LevelThree();
        System.out.println("How many do you need?");
        int num = in.nextInt();
        for (int i = 0;i<num;i++){
            String pro = a.JudgeAllpart();
            String pros = pro+"= ?";
            System.out.println(pros);
            Double answer = in.nextDouble();

            if (answer==Double.valueOf(b.Count(pro))){
                System.out.println("True");
                count++;
            }
            else
            {System.out.println("Wrong");
                System.out.println("The true is "+b.Count(pro));}
        }
        }
    }

