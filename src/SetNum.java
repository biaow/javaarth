import java.util.Random;

/**
 * Created by asus on 2017/5/18.
 */
public class SetNum {
    Random in = new Random();
    int a;
    int b;
    int c;

    public int getC() {
        setC();
        return c;
    }

    public void setC() {
        this.c = in.nextInt(3);
    }

    String [] Operator = {"+","-","*","/"};

    public int getB() {
        setB();
        return b;
    }

    public void setB() {
        this.b = in.nextInt(4);
    }

    public void setA() {
        this.a = in.nextInt(9)+1;
    }

    public int getA() {
        setA();
        return a;
    }
}
