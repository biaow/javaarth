public class RationalNumber
{
	private int numerator ,denominator;
	public RationalNumber (int number ,int denom)
	{
		if (denom == 0)
			denom =1;
		if (denom < 0)
		{
			number = number * -1;
			denom = denom * -1;
		}
		numerator = number;
		denominator = denom;
		reduce ();
//         	return numerator;
//		return denominator;
	}
	public int getNumerator ()
	{
		return numerator;
	}
	public int getDenominator ()
	{
		return denominator;
	}
	public RationalNumber reciprocal()
	{
		return new RationalNumber (denominator ,numerator);
	}

	//----------------------------------------------------------
	//Adds this rational number to the one passed as a parameter.
	// A common demoninator is found is found by multiplying the individual
	//denominators.
	//----------------------------------------------------------

	public RationalNumber add (RationalNumber op2)
	{
		int commonDenominator = denominator * op2.getDenominator();
		int numerator1 = numerator * op2.getDenominator();
		int numerator2 = op2.getNumerator() * denominator;
		int sum = numerator1 + numerator2;
		
		return new RationalNumber (sum , commonDenominator);
	}   

	public RationalNumber subtract (RationalNumber op2)
	{
		int commonDenominator = denominator * op2.getDenominator();
		int numerator1 = numerator * op2.getDenominator();
		int numerator2 = op2.getNumerator() * denominator;
		int difference = numerator1 - numerator2;
		return new RationalNumber (difference, commonDenominator);
	}
	public RationalNumber multiply (RationalNumber op2)
	{
		int numer = numerator * op2.getNumerator();
		int denom = denominator * op2.getDenominator();

		//int numerator2 = op2.getNumerator() * denominator;
		//int difference = numerator1 - numerator2;
		//return new RationalNumber (difference, commomDenominator);

		return new  RationalNumber (numer , denom);
	}

	public RationalNumber divide (RationalNumber op2)
	{
		return multiply (op2.reciprocal());
	}

	public boolean isLike (RationalNumber op2)
	{
		return ( numerator == op2.getNumerator() && denominator == op2.getDenominator());
	}


	public String toString()
	{
		String result;

		if(numerator == 0)
			result = "0";
		else
			if (denominator == 1)
				result = numerator + "";
			else
				result = numerator + "/" + denominator;

		return result;
	}


	private void reduce ()
	{ 
		if (numerator != 0)
		{
			int common = gcd (Math.abs(numerator),denominator);

			numerator = numerator / common;
			denominator = denominator / common;
		}
	}

	//--------------------------------------------------
	// Computes and return the greatest commom divisor of the two
	//--------------------------------------------------
	private int gcd (int num1,int num2)
	{
		while (num1 != num2)
			if (num1 > num2)
				num1 = num1 - num2;
			else
				num2 = num2 - num1;
		return num1;
	}
	public static void main(String[]args){
		RationalNumber score = new RationalNumber(1,2);
		RationalNumber score2 = new RationalNumber(2,3);
		String a =String.valueOf(score.add(score2));
		if (a.equalsIgnoreCase(String.valueOf(score.add(score2)) )){
		System.out.println(a);
	}}
}
	
