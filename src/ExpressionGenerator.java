import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 * Created by asus on 2017/5/30.
 */
public class ExpressionGenerator extends SetNum {
    public static void main(String args []) throws IOException{
        if (args.length != 3) {
            System.err.println("java ExpressionGenerator <number-of-expressions> <number-of-operator> <output-file-path>");
            System.exit(1);
        }
        OurAchv a = new OurAchv();
        int expr_num = Integer.parseInt(args[0]);
        int optr_num = Integer.parseInt(args[1]);
        String file_path = args[2];
        Postfix num = new Postfix();
        String line = null;
        FileWriter fw = new FileWriter(file_path);
        LinkedList<String> list = new LinkedList<>();
        StringBuilder arr = new StringBuilder() ;
        if (optr_num==1) {
            int count=0;
            for (int i = 0; i < expr_num; i++) {
                line = a.project(optr_num);
                list.add(line);
            }
            for (int i=0;i<expr_num;i++){
                line=list.get(i);
                        for(int j=0;j<expr_num;j++){
                            if (line==list.get(j)){
                                count++;
                                if (count>1){
                                    arr.append(j);
                                }
                            }

                        }

            }

            for (int i=0;i<expr_num;i++){

                for (int j=0;j<expr_num;j++){

                    if (j<arr.length()) {
                        if (i==arr.codePointAt(j)){
                            fw.write(a.project(optr_num));
                            fw.write('\n');
                        }
                    }
                    else {
                        fw.write(list.get(i));
                        fw.write('\n');
                    }

                }


            }
        }

        else if (optr_num==3) {
            int count=0;
            for (int i = 0; i < expr_num; i++) {
                line = a.project(optr_num);
                list.add(line);
            }
            for (int i=0;i<expr_num;i++){
                line=list.get(i);
                for(int j=0;j<expr_num;j++){
                    if (line==list.get(j)){
                        count++;
                        if (count>1){
                            arr.append(j);
                        }
                    }

                }

            }

            for (int i=0;i<expr_num;i++){

                for (int j=0;j<expr_num;j++){

                    if (j<arr.length()) {
                        if (i==arr.codePointAt(j)){
                            fw.write(a.project(optr_num));
                            fw.write('\n');
                        }
                    }
                    else {
                        fw.write(list.get(i));
                        fw.write('\n');
                    }

                }


            }
        }
        else {
            for (int i=0;i<expr_num;i++){
                fw.write(a.project(optr_num));
                fw.write('\n');
            }
        }
        fw.close();


    }











}
